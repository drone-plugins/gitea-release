import click
import json
import requests

from pathlib import Path

@click.command()
@click.argument("hostname", envvar="PLUGIN_GITEA_HOSTNAME", type=click.STRING)
@click.argument("repo_owner", envvar="PLUGIN_REPO_OWNER", type=click.STRING)
@click.argument("repo_name", envvar="PLUGIN_REPO_NAME", type=click.STRING)
@click.argument("release_tag", envvar="PLUGIN_RELEASE_TAG", type=click.STRING)
@click.option("release_target", "--target-commitish", envvar="PLUGIN_RELEASE_TARGET_COMMITISH", type=click.STRING)
@click.option("release_title", "--title", envvar="PLUGIN_RELEASE_TITLE", type=click.STRING)
@click.option("release_body", "--body", envvar="PLUGIN_RELEASE_BODY", type=click.STRING)
@click.option("release_is_draft", "--is-draft", envvar="PLUGIN_IS_DRAFT", is_flag=True, default=True)
@click.option("release_is_pre", "--is-prerelease", envvar="PLUGIN_IS_PRE_RELEASE", is_flag=True, default=True)
@click.argument("attachments", envvar="PLUGIN_ATTACHMENTS_PATH", type=click.Path(exists=True, readable=True, path_type=Path))
@click.option("--no-tls", envvar="PLUGIN_NO_TLS", is_flag=True, default=False)
@click.option("-u", "--username", envvar="PLUGIN_GITEA_USERNAME", type=click.STRING)
@click.option("-p", "--password", envvar="PLUGIN_GITEA_PASSWORD", type=click.STRING)
@click.option("-t", "--token", envvar="PLUGIN_GITEA_TOKEN", type=click.STRING)
def cli(
    hostname, repo_owner, repo_name, release_tag,
    release_target, release_title, release_body, release_is_draft, release_is_pre,
    attachments,
    no_tls,
    username, password, token
):
    url = f"http{'' if no_tls else 's'}://{hostname}/api/v1/repos/{repo_owner}/{repo_name}/releases"
    headers = {
        "Content-Type": "application/json"
    }
    auth = None
    data = json.dumps({
        "tag_name": release_tag,
        "name": release_title,
        "body": release_body,
        "draft": release_is_draft,
        "prerelease": release_is_pre,
        "target_commitish": release_target
    })

    if token is not None:
        click.echo("Using token authentication.")
        headers.update({
            "accept": "application/json",
            "Authorization": f"token {token}",
        })
    elif username is not None and password is not None:
        click.echo("Using basic http authentication.")
        auth = (username, password)
    else:
        click.echo("No credentials supplied.")

    r = requests.post(
        url,
        auth=auth,
        headers=headers,
        data=data
    )

    r.raise_for_status()

    release_id = r.json()["id"]

    click.echo("The release has been created.")

    if attachments is None:
        click.echo("No attachments to upload.")
        return

    url = f"http{'' if no_tls else 's'}://{hostname}/api/v1/repos/{repo_owner}/{repo_name}/releases/{release_id}/assets"
    files = {}

    if attachments.is_dir():
        for subpath in attachments.iterdir():
            if subpath.is_file():
                click.echo(f"Attaching {subpath.name}.")
                files[subpath.name] = open(subpath)
    elif attachments.is_file():
        click.echo(f"Attaching {attachments.name}.")
        files[attachments.name] = open(attachments)

    r = requests.post(
        url,
        auth=auth,
        headers=headers,
        data=data,
        files=files
    )

if __name__ == "__main__":
    cli()
