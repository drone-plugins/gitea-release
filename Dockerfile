FROM python:3.11-slim

LABEL org.opencontainers.image.title="gitea-release"
LABEL org.opencontainers.image.authors="git@facorazza.com"
LABEL org.opencontainers.image.url="https://gitlab.com/drone-plugins/gitea-release"
LABEL org.opencontainers.image.source="https://gitlab.com/drone-plugins/gitea-release"

# This prevents Python from writing out pyc files
ENV PYTHONDONTWRITEBYTECODE=1

# This keeps Python from buffering stdin/stdout
ENV PYTHONUNBUFFERED=1

# Create a new user
RUN adduser --uid 9999 --disabled-password bot
USER bot
WORKDIR /home/bot
COPY . .

# Update pip
RUN python -m pip --disable-pip-version-check install --user --upgrade pip

# Install requirements
RUN python -m pip install --user -r requirements.txt

CMD [ "python", "/home/bot/main.py" ]
